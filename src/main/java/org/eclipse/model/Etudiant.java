package org.eclipse.model;

public class Etudiant {
	
	private int id;
		private String prenom;
	private String nom;
	private String filiere;
	private String niveau;
	
	public Etudiant(String prenom, String nom, String filiere, String niveau) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.filiere = filiere;
		this.niveau = niveau;
	}

	public Etudiant() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getFiliere() {
		return filiere;
	}

	public void setFiliere(String filiere) {
		this.filiere = filiere;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	@Override
	public String toString() {
		return "Etudiant [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", filiere=" + filiere + ", niveau="
				+ niveau + "]";
	}
	
	
}
