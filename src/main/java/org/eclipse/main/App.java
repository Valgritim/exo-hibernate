package org.eclipse.main;

import org.eclipse.model.Etudiant;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Etudiant etudiant = new Etudiant("Jarjar","Beans","Congress","Ministre");
        Configuration config = new Configuration().configure();
        SessionFactory sf = config.buildSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(etudiant);
        transaction.commit();
        session.close();
        sf.close();
;        
    }
}
